/* Arduino libraries */
#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <SoftwareSerial.h>
//#include "mbed.h"

// appel de la bibliothèque /* Config */
//#include "config.h"//cette bibliothèque nesert pas à grand chose dans ce code là
#include <as5048.hpp> //bibliothèque de lecture bus I²C, créé spécifiquement pour les codeurs AS5048A
//#include "ams_as5048b.h"//bibliothèque I²C

AS5048A codeur_t_g; // translation
AS5048A codeur_t_d;
AS5048A codeur_r_g;
AS5048A codeur_r_d; // rotation

void setup() {
    // put your setup code here, to run once:

//___________________________________________
    /* Setup serial */
    Serial.setRx(PA3);
    Serial.setTx(PA2);
    Serial.begin(115200);
    //while (!Serial);
//-------------------------------------------

//___________________________________________
    /* Setup spi */
    SPI.setMOSI(PB5);
    SPI.setMISO(PB4);
    SPI.setSCLK(PB3);

    pinMode(PA6, OUTPUT);
	pinMode(PA7, OUTPUT);
	pinMode(PA8, OUTPUT);
	pinMode(PA9, OUTPUT);
    pinMode(PC9, OUTPUT);

	digitalWrite(PA6, HIGH);
	digitalWrite(PA7, HIGH);
	digitalWrite(PA8, HIGH);
	digitalWrite(PA9, HIGH);
	digitalWrite(PC9, HIGH);
    SPI.begin();
//-------------------------------------------

//___________________________________________
    /* I2C */
    Wire.setSDA(PB11);//active le Canal de discussion en fonction du PIN choisi
    Wire.setSCL(PB10);//active l'horloge du bus en fonction du PIN choisi
    Wire.begin();  
//-------------------------------------------


    /* Codeurs */
    codeur_t_g.setup_i2c(CAPT_L_GAUCHE);
    codeur_t_d.setup_i2c(CAPT_L_DROIT);
    codeur_r_g.setup_i2c(CAPT_R_GAUCHE);
    codeur_r_d.setup_i2c(CAPT_R_DROIT);
}

uint8_t m_bit_lecture = 0;

void loop() {
	static int i = 0;
	double recup;

    //test I²C

    recup = codeur_r_g.angle_degre();
    Serial.print("ANGLE_Capt.RG:");
    Serial.println(recup);
    delay(500);

    Serial.print("\n");

    recup = codeur_r_g.angle_radian();
    Serial.print("ANGLE_Capt.RG:");
    Serial.println(recup);
    delay(500);

    Serial.print("\n");


    i++;
}